#!/usr/bin/env python3

#Replace the following variable with the path to the .wav file you wish to play
WAV_FILE = "/home/user/Music"


import wave
import numpy as np
import alsaaudio as aa

from time import sleep
from struct import unpack
from sense_hat import SenseHat

sensehat = SenseHat()

white = (255,255,255)
black = (0,0,0)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)

#These are the colors displayed on the SENSE hat, one being the first row and eight being the last
one =(66,134,244) # light blue
two =(65,184,244)
three =(65,244,199)
four =(65,244,68)
five =(157,244,65)
six =(241,244,65)
seven =(244,246,65)
eight =(244,65,65) # almost red

#The range of colors to be displayed
spectrum = [one,two,three,four,five,six,seven,eight]

#Matrix reperesents each column in the display
matrix    = [0,0,0,0,0,0,0,0]

#The following two arrays are for calculations
power     = []
weighting = [2,8,8,16,16,32,32,64]

#Generates an array to reperesent each light on the SENSE hat
grid_layer = [white for p in range(64)]

#Another way of doing the above

#grid_layer = [] 
#for light in range(64):
#	grid_layer.append(white)

#replaces a light with a color on the SENSE hat
#Usage: replace(row of light, column of light, color in RGB format)
def replace(x,y,color):
    select = (((y-1)*8)-1) + (x)
    grid_layer[select] = color

#this is a "post" these lights turn on to signify that the program is running
replace(1,1,red)
replace(1,2,green)
replace(1,3,blue)

replace(2,1,red)
replace(2,2,green)
replace(2,3,blue)

replace(3,1,red)
replace(3,2,green)
replace(3,3,blue)

#after every change to the grid_layer senseht.set_pixels() must be called
sensehat.set_pixels(grid_layer)

sleep(3)

#The following is a courtesy of www.rototron.info
wavfile = wave.open(WAV_FILE)
sample_rate = wavfile.getframerate()
no_channels = wavfile.getnchannels()
chunk       = 4096 #must be a multiple of 8 

output = aa.PCM(aa.PCM_PLAYBACK, aa.PCM_NORMAL) #the next 5 lines set up the wav file to be played
output.setchannels(no_channels)
output.setrate(sample_rate)
output.setformat(aa.PCM_FORMAT_S16_LE)
output.setperiodsize(chunk)

# the following functions where taken from http://julip.co/2012/05/arduino-python-soundlight-spectrum/

def piff(val):
    return int(2*chunk*val/sample_rate)

def calculate_levels(data, chunk,sample_rate):
    global matrix

    # Convert raw data (ASCII string) to numpy array
    data = unpack("%dh"%(len(data)/2),data)
    data = np.array(data, dtype='h')

    # Apply FFT - real data
    fourier=np.fft.rfft(data)
    # Remove last element in array to make it the same size as chunk
    fourier=np.delete(fourier,len(fourier)-1)
    # Find average 'amplitude' for specific frequency ranges in Hz
    power = np.abs(fourier)   
    matrix[0]= int(np.mean(power[piff(0)    :piff(156):1]))
    matrix[1]= int(np.mean(power[piff(156)  :piff(313):1]))
    matrix[2]= int(np.mean(power[piff(313)  :piff(625):1]))
    matrix[3]= int(np.mean(power[piff(625)  :piff(1250):1]))
    matrix[4]= int(np.mean(power[piff(1250) :piff(2500):1]))
    matrix[5]= int(np.mean(power[piff(2500) :piff(5000):1]))
    matrix[6]= int(np.mean(power[piff(5000) :piff(10000):1]))
    matrix[7]= int(np.mean(power[piff(10000):piff(20000):1]))

    # Tidy up column values for the LED matrix
    matrix=np.divide(np.multiply(matrix,weighting),1000000)
    # Set floor at 0 and ceiling at 8 for LED matrix
    matrix=matrix.clip(0,8)
    return matrix

data = wavfile.readframes(chunk)
# Loop while audio data present



while data!='':#this is supposed to play the song until there is no song left to play
  
    output.write(data)   
    matrix=calculate_levels(data, chunk,sample_rate)
    grid_layer = [black for p in range(64)] 
    for y in range (0,8):
        for x in range(0, int(matrix[y])):
            replace(x+1,y,spectrum[x])
    sensehat.set_pixels(grid_layer)
    data = wavfile.readframes(chunk)
